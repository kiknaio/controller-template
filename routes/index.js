const express = require('express')
const router = express.Router()

router.get('/', (req, res, next) => {
  res.send('Chatbot by BOTfriends GmbH')
})

module.exports = router
