const axios = require('axios')
const { WEATHER_API_KEY } = require('../config')

const getTemperatureByCityName = async (cityName) => {
  try {
    const response = await axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${cityName}&units=metric&APPID=${WEATHER_API_KEY}`)
    return response.data ? Math.round(response.data.main.temp) : ''
  } catch(err) {
    console.log(err)
    return ''
  }
}

module.exports = {
  getTemperatureByCityName
}
