const axios = require('axios')
const { CMC_API_KEY } = require('../config')

const getCryptoTicker = async () => {
  try {
    const response = await axios.get(`https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest?CMC_PRO_API_KEY=${CMC_API_KEY}&symbol=BTC,LTC,XMR,ETH,XRP,ZEC`)
    return response.data ? Object.keys(response.data.data).map(currency => ({
      currency: symbolMapper[currency],
      price: response.data.data ? response.data.data[currency].quote.USD.price.toFixed(2) : 'not available'
    })) : ''
  } catch(err) {
    console.log(err)
    return ''
  }
}

const symbolMapper = {
  'BTC': 'Bitcoin',
  'ETH': 'Ethereum',
  'LTC': 'Litecoin',
  'XMR': 'Monero',
  'XRP': 'Ripple',
  'ZEC': 'Zcash'
}

module.exports = {
  getCryptoTicker
}