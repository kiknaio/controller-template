const { sendTextMessage, sendQuickReply, sendImageMessage, sendCardMessage } = require('./facebookService')

const handleMessages = async (messages, sender) => {
  const cardMessages = []
  messages.forEach(async message => {
    if (message.card) {
      cardMessages.push(message)
    } else {
      await handleMessage(message, sender)
    }
  })
  if (cardMessages.length) {
    await handleCardMessages(cardMessages, sender)
  }
}

const handleMessage = async (message, sender) => {
  switch (message.message) {
    case 'text':
      await message.text.text.map(async (text) => {
        if (text !== '') {
          await sendTextMessage(sender, text)
        }
      })
      break
    case 'quickReplies':
      let replies = []
      message.quickReplies.quickReplies.forEach((text) => {
        const reply =
          {
            'content_type': 'text',
            'title': text,
            'payload': text
          }
        replies.push(reply)
      })
      await sendQuickReply(sender, message.quickReplies.title, replies)
      break
    case 'image':
      await sendImageMessage(sender, message.image.imageUri)
      break
  }
}

const handleCardMessages = async (cardMessages, sender) => {
  const elements = []
  cardMessages.forEach(message => {
    const { title, subtitle, imageUri } = message.card
    const buttons = []
    for (let index = 0; index < message.card.buttons.length; index++) {
      const isLink = (message.card.buttons[index].postback.substring(0, 4) === 'http')
      let button
      if (isLink) {
        button = {
          'type': 'web_url',
          'title': message.card.buttons[index].text,
          'url': message.card.buttons[index].postback
        }
      } else {
        button = {
          'type': 'postback',
          'title': message.card.buttons[index].text,
          'payload': message.card.buttons[index].postback || message.card.buttons[index].text
        }
      }
      buttons.push(button)
    }
    elements.push({
      title,
      subtitle,
      image_url: imageUri,
      buttons
    })
  })
  await sendCardMessage(sender, elements)
}

module.exports = {
  handleMessage,
  handleMessages,
  handleCardMessages
}
