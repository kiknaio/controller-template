const { FB_PAGE_TOKEN } = require('../config')
const axios = require('axios')
const qs = require('querystring')
const { isDefined } = require('../utils/helper')

const callSendAPI = async messageData => {
  try {
    const response = await axios.post('https://graph.facebook.com/v3.2/me/messages?' + qs.stringify({ access_token: FB_PAGE_TOKEN }), messageData)
    const { data } = response
    if (data.message_id) {
      console.log(`Successfully sent message with id ${data.message_id} to recipient ${data.recipient_id}`)
    } else {
      console.log(`Successfully sent Send API for recipient ${data.recipient_id}`)
    }
  } catch (err) {
    console.error('Failed calling Send API', err)
  }
}

const sendTextMessage = async (recipientId, text) => {
  const messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: text
    }
  }
  await callSendAPI(messageData)
}

const sendTyping = async (recipientId, indicator) => {
  const messageData = {
    recipient: {
      id: recipientId
    },
    sender_action: `typing_${indicator}`
  }
  await callSendAPI(messageData)
}

const sendQuickReply = async (recipientId, text, replies, metadata) => {
  const messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: text,
      metadata: isDefined(metadata) ? metadata : '',
      quick_replies: replies
    }
  }
  await callSendAPI(messageData)
}

const sendImageMessage = async  (recipientId, imageUrl) => {
  const messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: 'image',
        payload: {
          url: imageUrl
        }
      }
    }
  }
  await callSendAPI(messageData)
}

const sendCardMessage = async (recipientId, elements) => {
  const messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: 'template',
        payload: {
          template_type: 'generic',
          elements: elements
        }
      }
    }
  }
  await callSendAPI(messageData)
}

module.exports = {
  sendTextMessage,
  sendTyping,
  sendQuickReply,
  sendImageMessage,
  sendCardMessage
}
