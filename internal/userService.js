const { levelInsert, levelSelect } = require('../storage/level')

const registerUser = async (sender, name) => {
  const result = await levelInsert(sender, name)
  if (result.success) {
    return `User ${name} was registered.`
  } else {
    return `Couldn\'t register user. Error: ${result.message}`
  }
}

const showName = async sender => {
  const result = await levelSelect(sender)
  if (result.success) {
    return `Your name is ${result.value}.`
  } else {
    return `Couldn\'t find user. Error: ${result.message}`
  }
}

module.exports = {
  registerUser,
  showName
}
