const express = require('express')
const logger = require('morgan')
const bodyParser = require('body-parser')
const { checkEnvVariables } = require('./utils/settings')

const indexRouter = require('./routes/index')
const webhookRouter = require('./routes/webhook')

const app = express()

checkEnvVariables()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: false
}))
app.use(logger('dev'))

app.use('/', indexRouter)
app.use('/', webhookRouter)

const port = process.env.PORT || '3000'
app.set('port', port)

app.listen(app.get('port'), function () {
  console.log('BOTfriends Controller running on port', app.get('port'))
})
